#!/usr/bin/env python3

import matplotlib.pyplot as plt
import pandas as pd

import tensorflow as tf
from keras.models import Sequential
from keras.layers.core import Activation, Dense
from keras.layers.recurrent import LSTM
from keras.optimizers import Adam
from keras.layers import Dropout
from keras.optimizers import RMSprop
import numpy as np
import random

from sklearn.model_selection import train_test_split

input_dim = 6                # 入力データの次元数：実数値1個なので1を指定
output_dim = 1               # 出力データの次元数：同上
num_hidden_units = 64       # 隠れ層のユニット数
len_sequence = 16            # 時系列の長さ
batch_size = 32             # ミニバッチサイズ
num_of_training_epochs = 100 # 学習エポック数
learning_rate = 0.001        # 学習率
num_training_samples = 0  # 学習データのサンプル数

def ReadData(readFileName, readFileDir):
    
    price = pd.read_csv(readFileDir+readFileName, delimiter=",", header=[1, 1], encoding="SHIFT-JIS")
    return price

def VisualizeData(price):
    # Delete Top row
    price.columns = price.columns.droplevel(0)
    n = np.arange(0, len(price))

    # Price Data Visualization
    plt.subplot(4,1,1)
    plt.title('day - Price')
    plt.plot(n, price['始値'], n, price['終値'])

    plt.subplot(4,1,2)
    plt.plot(n, price['高値'], n, price['安値'])

    plt.subplot(4,1,3)
    plt.plot(n, price['出来高'])

    plt.subplot(4,1,4)
    plt.plot(n, price['終値調整値'])

    plt.draw()
    plt.waitforbuttonpress(0.0)
    plt.close()

def MakeFeature(price):
    # 要素数の設定
    num_training_samples = len(price)
    

    # Make Label Data
    modified_data = []
    price_diff = []
    label = []
    for i in range(0, num_training_samples):
        price_diff.append(int(price.loc[i,'終値']) - int(price.loc[i,'始値']))

    # Make Trainning data
    # LSTMに与える入力は (サンプル, 時刻, 特徴量の次元) の3次元になる。
    train_data = np.empty([num_training_samples - len_sequence + 1, len_sequence, input_dim])
    for i in range(len_sequence-1, num_training_samples):
        feature = []
        for j in range(len_sequence):
            feature.append([int(price.loc[i-j, '始値']), 
                int(price.loc[i,'終値']), 
                int(price.loc[i,'高値']), 
                int(price.loc[i,'安値']), 
                int(price.loc[i,'終値']) - int(price.loc[i-j, '始値']), 
                int(price.loc[i,'高値']) - int(price.loc[i-j, '安値'])])
            #feature.append(int(price.loc[i-j, '始値']))

        feature_dim = len(feature[0])
        if (feature_dim != input_dim):
            print("Error feature_dim must be equal to input dim")
        #train_data.append([feature])
        train_data[i-len_sequence, :, :] = np.array(feature)

        if price_diff[i] > 0.0:
            #label.append([1, 0])
            label.append(1)
        else:
            #label.append([0, 1])
            label.append(0)

    label = np.array(label)
    print(label.shape)
    #train_data = train_data.reshape([len(train_data), feature_dim])
    return train_data.reshape((num_training_samples - len_sequence + 1, len_sequence, feature_dim)), label



# Not in use
def Features(price):

    # 要素数の設定
    count_s = len(price)

    modified_data = []
    price_diff = []
    for i in range(0,count_s):
        price_diff.append(int(price.loc[i,'終値']) - int(price.loc[i,'始値']))

        #modified_data.append(float(price.loc[i,['終値']] - price.loc[i-1,['終値']])/float(price.loc[i,['終値']]))

    # 要素数の設定
    count_m = len(price_diff)
     
    # 過去４日分の上昇率のデータを格納するリスト
    train_data = []
     
    # 正解値を格納するリスト　価格上昇: 1 価格低下:0
    label = []

    #連続の上昇率のデータを格納していく
    day_term = 14
    for i in range(day_term, count_m):

        feature = []
        for j in range(day_term):
            feature.append(int(price.loc[i-j, '始値']))
            #feature.append(int(price.loc[i-j, '終値']))
            #feature.append(int(price.loc[i-j, '高値']))
            #feature.append(int(price.loc[i-j, '安値']))
            #feature.append(int(price.loc[i-j, '出来高']))
            #feature.append(int(price.loc[i-j, '終値']) - int(price.loc[i-j-1, '始値']))
            #feature.append(int(price.loc[i-j, '終値']) - int(price.loc[i-j-1, '終値']))
            #feature.append(int(price.loc[i-j, '高値']) - int(price.loc[i-j-1, '高値']))
            #feature.append(int(price.loc[i-j, '安値']) - int(price.loc[i-j-1, '安値']))
            #feature.append(int(price.loc[i-j, '出来高']) - int(price.loc[i-j-1, '出来高']))

        feature_dim = len(feature)
        input_dim = feature_dim
        train_data.append([feature])
        # 上昇率が0以上なら1、そうでないなら0を格納
        if price_diff[i] > 0.0:
            label.append(1)
        else:
            label.append(0)
    
    train_data = np.array(train_data)
    print(train_data.shape)
    label = np.array(label)

    train_data = train_data.reshape([len(train_data), feature_dim])

    return train_data, label

def trial(readFileName, readFileDir):
    price = ReadData(readFileName, readFileDir)
    #train_data, label = Features(price)
    train_data, label = MakeFeature(price)
    train_score_list = []
    test_score_list = []
    # データの分割（データの80%を訓練用に、20％をテスト用に分割する）
    #X_train, X_test, y_train, y_test =train_test_split(train_data, label, train_size=0.8,test_size=0.2,random_state=2)
    # LSTMに与える入力は (サンプル, 時刻, 特徴量の次元) の3次元になる。
    
    # モデル構築
    model = Sequential()
    model.add(LSTM(
    num_hidden_units,
    input_shape=(len_sequence, input_dim),
    return_sequences=False))
    #model.add(Dropout(0.2))
    model.add(Dense(output_dim))
    #model.add(Activation("softmax"))
    model.compile(loss="mean_squared_error", optimizer=RMSprop(lr=learning_rate), metrics = ['accuracy'])
    model.summary()

    # 学習
    fit = model.fit(
    train_data, label,
    batch_size=batch_size,
    epochs=num_of_training_epochs,
    validation_split=0.2
    )
    # ----------------------------------------------
    # Some plots
    # ----------------------------------------------
    fig, (axL, axR) = plt.subplots(ncols=2, figsize=(10,4))

    # loss
    def plot_history_loss(fit):
        # Plot the loss in the history
        axL.plot(fit.history['loss'],label="loss for training")
        axL.plot(fit.history['val_loss'],label="loss for validation")
        axL.set_title('model loss')
        axL.set_xlabel('epoch')
        axL.set_ylabel('loss')
        axL.legend(loc='upper right')

    # acc
    def plot_history_acc(fit):
        # Plot the loss in the history
        axR.plot(fit.history['acc'],label="loss for training")
        axR.plot(fit.history['val_acc'],label="loss for validation")
        axR.set_title('model accuracy')
        axR.set_xlabel('epoch')
        axR.set_ylabel('accuracy')
        axR.legend(loc='upper right')

    plot_history_loss(fit)
    #plot_history_acc(fit)
    _strSaveFigName = readFileName + '.png'
    fig.savefig(_strSaveFigName)
    plt.close()




#readFileName = '1386_2019.csv'
readFileDir = '/home/takeshi/Data/StockUDPrediction/2019-20200817T135327Z-001/2019/'
company_list = pd.read_csv('CompanyList.txt', delimiter=" ", header=None,  encoding="SHIFT-JIS")

comp_count = len(company_list)
for i in range(comp_count):
    #print(company_list.loc[i, 0])
    readFileName = str(company_list.loc[i, 0])
    print(readFileName)
    trial(readFileName, readFileDir)
