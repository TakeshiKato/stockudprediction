# coding: utf-8
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

readFileName = '1386_2019.csv'
readFileDir = '/home/takeshi/Data/StockUDPrediction/2019-20200817T135327Z-001/2019/'
price = pd.read_csv(readFileDir+readFileName, delimiter=",", header=[1, 1], encoding="SHIFT-JIS")

# Delete Top row
price.columns = price.columns.droplevel(0)
n = np.arange(0, len(price))

# Price Data Visualization
plt.subplot(4,1,1)
plt.title('day - Price')
plt.plot(n, price['始値'], n, price['終値'])

plt.subplot(4,1,2)
plt.plot(n, price['高値'], n, price['安値'])

plt.subplot(4,1,3)
plt.plot(n, price['出来高'])

plt.subplot(4,1,4)
plt.plot(n, price['終値調整値'])

plt.draw()
plt.waitforbuttonpress(0.0)
plt.close()

# 要素数の設定
count_s = len(price)


print(price.loc[1,'終値'])
print(price.loc[1,'始値'])
print(price.loc[1,'終値'] - price.loc[1,'始値'])
modified_data = []
price_diff = []
for i in range(0,count_s):
	price_diff.append(price.loc[i,'終値'] - price.loc[i,'始値'])

	#modified_data.append(float(price.loc[i,['終値']] - price.loc[i-1,['終値']])/float(price.loc[i,['終値']]))

# 要素数の設定
count_m = len(price_diff)
 
# 過去４日分の上昇率のデータを格納するリスト
train_data = []
 
# 正解値を格納するリスト　価格上昇: 1 価格低下:0
label = []

#連続の上昇率のデータを格納していく
day_term = 8
for i in range(day_term-1, count_m):

	feature = []
	for j in range(day_term):
		feature.append(price.loc[i-j, '始値'])
		feature.append(price.loc[j-j, '終値'])
		feature.append(price.loc[j-j, '高値'])
		feature.append(price.loc[j-j, '安値'])
		feature.append(price.loc[j-j, '終値'] - price.loc[j-j, '始値'])
	feature_dim = len(feature)
	train_data.append([feature])
	# 上昇率が0以上なら1、そうでないなら0を格納
	if price_diff[i] > 0:
		label.append(1)
	else:
		label.append(0)

print(count_m)
print(len(feature))
print(len(train_data))
print(train_data[0])

train_data = np.array(train_data)
label = np.array(label)

print(train_data.shape)
print(label.shape)
train_data = train_data.reshape([len(train_data), feature_dim])
# データの分割（データの80%を訓練用に、20％をテスト用に分割する）
X_train, X_test, y_train, y_test =train_test_split(train_data, label, train_size=0.8,test_size=0.2,random_state=2)

# サポートベクターマシーン
clf = svm.LinearSVC()
# サポートベクターマシーンによる訓練
clf.fit(X_train , y_train)
 
# 学習後のモデルによるテスト
# トレーニングデータを用いた予測
y_train_pred = clf.predict(X_train)
# テストデータを用いた予測
y_val_pred = clf.predict(X_test)
 
# 正解率の計算
train_score = accuracy_score(y_train, y_train_pred)
test_score = accuracy_score(y_test, y_val_pred)
 
# 正解率を表示
print("トレーニングデータに対する正解率：" + str(train_score * 100) + "%")
print("テストデータに対する正解率：" + str(test_score * 100) + "%")
