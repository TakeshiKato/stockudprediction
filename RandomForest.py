# coding: utf-8
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

#from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


def ReadData(readFileName, readFileDir):
	
	price = pd.read_csv(readFileDir+readFileName, delimiter=",", header=[1, 1], encoding="SHIFT-JIS")
	return price

def VisualizeData(price):
	# Delete Top row
	price.columns = price.columns.droplevel(0)
	n = np.arange(0, len(price))

	# Price Data Visualization
	plt.subplot(4,1,1)
	plt.title('day - Price')
	plt.plot(n, price['始値'], n, price['終値'])

	plt.subplot(4,1,2)
	plt.plot(n, price['高値'], n, price['安値'])

	plt.subplot(4,1,3)
	plt.plot(n, price['出来高'])

	plt.subplot(4,1,4)
	plt.plot(n, price['終値調整値'])

	plt.draw()
	plt.waitforbuttonpress(0.0)
	plt.close()

def Features(price):

	# 要素数の設定
	count_s = len(price)

	modified_data = []
	price_diff = []
	for i in range(0,count_s):
		price_diff.append(int(price.loc[i,'終値']) - int(price.loc[i,'始値']))

		#modified_data.append(float(price.loc[i,['終値']] - price.loc[i-1,['終値']])/float(price.loc[i,['終値']]))

	# 要素数の設定
	count_m = len(price_diff)
	 
	# 過去４日分の上昇率のデータを格納するリスト
	train_data = []
	 
	# 正解値を格納するリスト　価格上昇: 1 価格低下:0
	label = []

	#連続の上昇率のデータを格納していく
	day_term = 14
	for i in range(day_term, count_m):

		feature = []
		for j in range(day_term):
			feature.append(int(price.loc[i-j, '始値']))
			feature.append(int(price.loc[i-j, '終値']))
			feature.append(int(price.loc[i-j, '高値']))
			feature.append(int(price.loc[i-j, '安値']))
			feature.append(int(price.loc[i-j, '出来高']))
			feature.append(int(price.loc[i-j, '終値']) - int(price.loc[i-j-1, '始値']))
			feature.append(int(price.loc[i-j, '終値']) - int(price.loc[i-j-1, '終値']))
			feature.append(int(price.loc[i-j, '高値']) - int(price.loc[i-j-1, '高値']))
			feature.append(int(price.loc[i-j, '安値']) - int(price.loc[i-j-1, '安値']))
			feature.append(int(price.loc[i-j, '出来高']) - int(price.loc[i-j-1, '出来高']))


		feature_dim = len(feature)
		train_data.append([feature])
		# 上昇率が0以上なら1、そうでないなら0を格納
		if price_diff[i] > 0.0:
			label.append(1)
		else:
			label.append(0)

	train_data = np.array(train_data)
	label = np.array(label)

	train_data = train_data.reshape([len(train_data), feature_dim])

	return train_data, label

def trial(readFileName, readFileDir):
	price = ReadData(readFileName, readFileDir)
	train_data, label = Features(price)
	train_score_list = []
	test_score_list = []
	for iteration in range(50):
		# データの分割（データの80%を訓練用に、20％をテスト用に分割する）
		X_train, X_test, y_train, y_test =train_test_split(train_data, label, train_size=0.8,test_size=0.2,random_state=2)

		#clf = tree.DecisionTreeClassifier()
		clf = RandomForestClassifier(random_state=0)
		# 学習させる
		clf.fit(X_train, y_train)
		 
		# 学習後のモデルによるテスト
		# トレーニングデータを用いた予測
		y_train_pred = clf.predict(X_train)
		# テストデータを用いた予測
		y_val_pred = clf.predict(X_test)
		 
		# 正解率の計算
		train_score = accuracy_score(y_train, y_train_pred)
		test_score = accuracy_score(y_test, y_val_pred)
		
		train_score_list.append(train_score)
		test_score_list.append(test_score)

		# 正解率を表示
		#print("Train Score：" + str(train_score * 100) + "%")
		#print("Test Score：" + str(test_score * 100) + "%")

	train_score_ave = np.mean(np.array(train_score))
	test_score_ave = np.mean(np.array(test_score))
	print(readFileName)
	print("Average Train Score：" + str(train_score_ave * 100) + "%")
	print("Average Test Score：" + str(test_score_ave * 100) + "%")


#readFileName = '1386_2019.csv'
readFileDir = '/home/takeshi/Data/StockUDPrediction/2019-20200817T135327Z-001/2019/'

company_list = pd.read_csv('CompanyList.txt', delimiter=" ", header=None,  encoding="SHIFT-JIS")

count_s = len(company_list)
for i in range(count_s):
	#print(company_list.loc[i, 0])
	readFileName = str(company_list.loc[i, 0])
	print(readFileName)
	trial(readFileName, readFileDir)